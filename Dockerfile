# FROM oracle:7
FROM registry-dev.rxcorp.com/latam/llatamoracle:latest
MAINTAINER Kaspar, Alejandro "kaspar.alejandro@gmail.com"


RUN cd /

#OS SETUP PYTHON3, PYTHON2, OTHER DEV TOOLS
RUN yum install -y oracle-epel-release-el7
RUN yum install -y dos2unix
RUN yum install -y wget
RUN yum install -y gcc openssl redhat-rpm-config
RUN yum-config-manager --disable ol7_latest
RUN yum install -y python36 python36-devel python36-setuptools python3-pip git
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python /get-pip.py
RUN update-alternatives --install /usr/bin/python python /usr/bin/python2.7 2
RUN yum-config-manager --enable ol7_latest
RUN yum install -y yum-utils
RUN yum-config-manager --enable *EPEL
ENV SLUGIFY_USES_TEXT_UNIDECODE=yes
RUN yum install java-1.8.0-openjdk-devel -y
RUN yum install java-1.8.0-openjdk -y
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.6 3

#KAFKA DOWNLOAD
RUN wget --no-check-certificate https://dlcdn.apache.org/kafka/3.0.0/kafka-3.0.0-src.tgz
RUN ls
RUN tar -xzf kafka-3.0.0-src.tgz
RUN ln -s kafka-3.0.0 kafka

RUN mkdir -p /usr/local/data-infra-challenge

COPY . /usr/local/data-infra-challenge/

RUN ls /usr/local/data-infra-challenge/

RUN python3 -m pip install -r /usr/local/data-infra-challenge/stream-client/requirements.txt

EXPOSE 2181 9092

RUN dos2unix /usr/local/data-infra-challenge/stream-client/scripts/entrypoint.sh
RUN cp /usr/local/data-infra-challenge/stream-client/scripts/entrypoint.sh /usr/local/bin/entrypoint.sh
RUN cp /usr/local/data-infra-challenge/stream-client/scripts/entrypoint.sh /usr/bin/entrypoint.sh
RUN ls /usr/local/data-infra-challenge/stream-client/scripts/entrypoint.sh
RUN chmod 755 /usr/local/data-infra-challenge/stream-client/scripts/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]
#!/usr/bin/bash
echo "export PATH=$PATH:/root/kafka-3.0.0/bin" >> ~/.bash_profile
source ~/.bash_profile
#START ZOOKEEPER
zookeeper-server-start.sh -daemon /root/kafka/config/zookeeper.properties
#START KAFKA
kafka-server-start.sh -daemon /root/kafka/config/server.properties

python /usr/local/data-infra-challenge/producer.py > /usr/local/data-infra-challenge/logs/producer.log

python /usr/local/data-infra-challenge/consumer.py > /usr/local/data-infra-challenge/logs/consumer.log

/bin/sh
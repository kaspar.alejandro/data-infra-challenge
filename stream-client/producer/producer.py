from time import sleep
import json
from pathlib import Path
from kafka import KafkaProducer

def produce_payments_data(producer: KafkaProducer):
   p_file = Path(__file__).resolve().parent.parent.joinpath("data/payments.json")
   with open(p_file) as json_file:
       data = json.load(json_file)
       for record in data:
           print(record)
           producer.send('topic_test', value=record)
           sleep(0.5)

def produce_rentals_data(producer: KafkaProducer):
   p_file = Path(__file__).resolve().parent.parent.joinpath("data/rentals.json")
   with open(p_file) as json_file:
       data = json.load(json_file)
       for record in data:
           print(record)
           producer.send('topic_test', value=record)
           sleep(0.5)

if __name__ == '__main__':
     producer = KafkaProducer(
         bootstrap_servers=['localhost:9092'],
         value_serializer=lambda x: json.dumps(x).encode('utf-8'))
     produce_payments_data(producer)
     produce_rentals_data(producer)